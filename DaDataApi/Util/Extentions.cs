﻿using Microsoft.Extensions.DependencyInjection;

namespace DaDataApiService.Util {
    public static class Extentions {
        /// <summary>
        /// Внедряет Scoped <see cref="DadataApi"/>
        /// </summary>
        /// <param name="services"></param>
        /// <param name="token">Api ключ профиля dadata.ru</param>
        public static void AddDadataApi(this IServiceCollection services, string token) {
            services.AddScoped(provider => new DadataApiOptions() { 
                Token = token
            });
            services.AddScoped<DadataApi, DadataApi>();
        }
    }
}
