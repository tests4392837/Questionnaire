﻿using Dadata;

namespace DaDataApiService {
    /// <summary>
    /// Служит оберткой над <see cref="SuggestClientAsync"/>
    /// </summary>
    /// <remarks>Можно было бы не заморачиваться и пользоваться обычным <see cref="SuggestClientAsync"/>, но тогда в каждом контроллере придется указывать один и тот же api ключ. 
    /// Поэтому сделал эту обертку и добавил возможность использовать DI
    /// </remarks>
    public class DadataApi {
        private readonly DadataApiOptions _options;
        
        public SuggestClientAsync suggestAsync { get; private set; }

        public DadataApi(DadataApiOptions options) {
            _options = options;
            suggestAsync = new SuggestClientAsync(_options.Token);
        }
    }
}
