﻿namespace DaDataApiService {
    /// <summary>
    /// Параметры для инициализации
    /// </summary>
    public class DadataApiOptions {
        private string _token;

        /// <summary>
        /// API ключ dadata.ru. При попытке установить пустую строку или null выбросит исключение
        /// </summary>
        public string Token {
            get => _token;
            set {
                if (string.IsNullOrEmpty(value)) throw new Exception("Указан некорректный токен");
                _token = value;
            }
        }
    }
}
