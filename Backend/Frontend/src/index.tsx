import ReactDOM from 'react-dom/client';
import App from './app';

import "../src/assets/css/templates.css"
import "../src/assets/css/components.css"
import "../src/assets/css/styles.css"

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <App/>
);