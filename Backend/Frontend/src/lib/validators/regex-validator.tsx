export function validateRegex(value:string, pattern:string){
    const reg = new RegExp(pattern);
    return reg.test(value);
}
export function validateRegexArray(arr:Array<{value:string, pattern:string}>){
    for(var i = 0; i < arr.length; i++){
        var reg = new RegExp(arr[i].pattern);
        var res = reg.test(arr[i].value);

        if(!res) return false;
    }
    return true;
}