import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { store } from './redux-storage';
import React from 'react';
import QuestionnaireRoute from './routes/questionnaire';
import { GlobalLoaderProvider } from '../components/global-loader';


function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <React.Fragment>
                    <Routes>
                        <Route path="/questionnaire/:p_step?" element={<QuestionnaireRoute/>}/>
                    </Routes>
                    <GlobalLoaderProvider/>
                </React.Fragment>
            </BrowserRouter>
        </Provider>
    );
}

export default App;
