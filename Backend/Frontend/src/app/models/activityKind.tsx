export default interface ActivityKind {
    id:string,
    name:string,
    isLegal:boolean
}