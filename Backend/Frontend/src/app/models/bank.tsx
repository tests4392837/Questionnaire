export default interface Bank{
    bic:string,
    name:string,
    checkingAccount:string,
    correspondentAccount:string
}