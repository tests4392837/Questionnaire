import Checkbox from "../../../components/checkbox";
import CustomInput from "../../../components/custom-input";
import React from "react";
import FilePicker, { IFilePickerResult } from "../../../components/file-picker";
import { setActivityKind, setLegalEgripScan, setLegalInn, setLegalInnScan, setLegalName, setLegalOgrn, setLegalOgrnScan, setLegalRegisterDate, setLegalRentaContract, setLegalRentaScan, setLegalShortName, setNonlegalEgripScan, setNonlegalInn, setNonlegalInnScan, setNonlegalOgrnip, setNonlegalOgrnipScan, setNonlegalRegisterDate, setNonlegalRentaContract, setNonlegalRentaScan, useStore } from "../../redux-storage";
import backend from "../../api/backend";
import { useDispatch } from "react-redux";
import moment from "moment";
import Dropdown from "../../../components/dropdown";
import ActivityKind from "../../models/activityKind";
import { globalLoader } from "../../../components/global-loader";

const QuestionnaireStep1 = () => {
    const store = useStore();
    const nonLegal = store.routes.questionnaire.nonlegal;
    const legal = store.routes.questionnaire.legal;

    const dispatch = useDispatch();

    /**
     * Обработчик выбора формы собственности
     * @param value Новое значение
     */
    function activityKindChangeHandle(value:ActivityKind){
        dispatch(setActivityKind(value));
    }

    // nonlegal handlers

    /**
     * Обработчик изменения инн
     * @param value Новое значение
     */
    function nonlegalInnChangeHandle(value:string){
        dispatch(setNonlegalInn(value));
    }

    /**
     * Обработчик изменения скана ИНН
     * @param value Новое значение
     */
    function nonlegalInnScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setNonlegalInnScan(value));
    }

    /**
     * Обработчик изменения ОГРНИП
     * @param value Новое значение
     */
    function nonlegalOgrnipChangeHandle(value:string){
        dispatch(setNonlegalOgrnip(value));
    }

    /**
     * Обработчик изменения ОГРНИП
     * @param value Новое значение
     */
    function nonlegalOgrnipScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setNonlegalOgrnipScan(value));
    }

    /**
     * Обработчик изменения даты регистрации
     * @param value Новое значение
     */
    function nonlegalRegisterDateChangeHandle(value:string){
        dispatch(setNonlegalRegisterDate(value));
    }

    /**
     * Обработчик изменения скана ИГРИП
     * @param value Новое значение
     */
    function nonlegalEgripScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setNonlegalEgripScan(value));
    }

    /**
     * Обработчик изменения скана договора аренды
     * @param value Новое значение
     */
    function nonlegalRentaScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setNonlegalRentaScan(value));
    }

    /**
     * Обработчик изменения наличия договора аренды
     * @param value Новое значение
     */
    function nonlegalRentaChangeHandle(value:boolean){
        dispatch(setNonlegalRentaContract(value));
    }



    // legal handlers

    /**
     * Обработчик изменения наименования организации
     * @param value Новое значение
     */
    function legalNameChangeHandle(value:string){
        dispatch(setLegalName(value));
    }


    /**
     * Обработчик изменения сокращенного наименования организации
     * @param value Новое значение
     */
    function legalShortNameChangeHandle(value:string){
        dispatch(setLegalShortName(value));
    }


    /**
     * Обработчик изменения инн
     * @param value Новое значение
     */
    function legalInnChangeHandle(value:string){
        dispatch(setLegalInn(value));
        if(value.length == 10){
            loadLegalInfo(value);
        }
    }

    /**
     * Обработчик изменения скана инн
     * @param value Новое значение
     */
    function legalInnScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setLegalInnScan(value));
    }

    /**
     * Обработчик изменения огрн
     * @param value Новое значение
     */
    function legalOgrnChangeHandle(value:string){
        dispatch(setLegalOgrn(value));
    }

    /**
     * Обработчик изменения скана огрн
     * @param value Новое значение
     */
    function legalOgrnScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setLegalOgrnScan(value));
    }

    /**
     * Обработчик изменения даты регистрации
     * @param value Новое значение
     */
    function legalRegisterDateChangeHandle(value:string){
        dispatch(setLegalRegisterDate(value));
    }

    /**
     * Обработчик изменения скана ЕГРИП
     * @param value Новое значение
     */
    function legalEgripScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setLegalEgripScan(value));
    }

    /**
     * Обработчик изменения скана договора аренды
     * @param value Новое значение
     */
    function legalRentaScanChangeHandle(value:IFilePickerResult|undefined){
        dispatch(setLegalRentaScan(value));
    }

    /**
     * Обработчик изменения наличия договора аренды
     * @param value Новое значение
     */
    function legalRentaChangeHandle(value:boolean){
        dispatch(setLegalRentaContract(value));
    }

    /**
     * Получает и устанавливает полное наименование, сокращенное наименование, дату регистрации и ОГРН по ИНН организации
     * @param value ИНН организации
     */
    function loadLegalInfo(value:string){
        globalLoader.show();
        
        backend.getCompanyInfo({query: value}).then(res => {
            if(res.success){
                legalNameChangeHandle(res.data.name)
                legalShortNameChangeHandle(res.data.shortName);
                legalRegisterDateChangeHandle(moment(res.data.registerDate).format("YYYY-MM-DD"));
                legalOgrnChangeHandle(res.data.ogrn);
            }
        }).finally(() => {
            globalLoader.hide();
        });
    }

    const renderedActivityKind = (item:ActivityKind) => item.name;
    
    return(
        <React.Fragment>
            <div className="block-title">Форма собственности</div>

            <div className="row">
                <div className="col-12 col-md-6">
                    <Dropdown placeholder="Выберите из списка" list={store.activityKinds} datatemplate={renderedActivityKind} value={store.routes.questionnaire.activityKind} onChange={activityKindChangeHandle}/>
                </div>
            </div>

            <div className="big-arrow down">➔</div>

            <div>
                <div className="block-title">{store.routes.questionnaire.activityKind ? store.routes.questionnaire.activityKind.name : ""}</div>

                {store.routes.questionnaire.activityKind?.isLegal ?
                <div>
                    <div className="row">
                        <div className="col-12 col-md-6" style={{marginBottom: 24}}>
                            <div className="interactable--label">Наименование полное*</div>
                            <CustomInput required placeholder="ООО «Московская промышленная компания»" maxLength={100} value={legal.name} onChange={legalNameChangeHandle}/>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className="row">
                                <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                    <div className="interactable--label">Наименование сокращенное*</div>
                                    <CustomInput required placeholder="ООО «МПК»" maxLength={50} value={legal.shortName} onChange={legalShortNameChangeHandle}/>
                                </div>
                                <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                    <div className="interactable--label">Дата регистрации*</div>
                                    <CustomInput required type="date" max="9999-01-01" placeholder="дд.мм.гггг" tip="Дата должна быть указана в формате ДД.ММ.ГГГГ" value={legal.registerDate} onChange={legalRegisterDateChangeHandle}/>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-6">
                            <div className="row">
                                <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                    <div className="interactable--label">ИНН*</div>
                                    <CustomInput required placeholder="хххххххххх" pattern="[0-9]{10,10}" maxLength={10} tip="Поле должно содержать 10 цифр" value={legal.inn} onChange={legalInnChangeHandle}/>
                                </div>
                                <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                    <div className="interactable--label">Скан ИНН*</div>
                                    <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={legal.innScan} onChange={legalInnScanChangeHandle}/>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className="row align-items-center">
                                <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                    <div className="interactable--label">ОГРН*</div>
                                    <CustomInput required placeholder="ххххххххххххх" pattern="[0-9]{13,13}" maxLength={13}  tip="Поле должно содержать 13 цифр" value={legal.ogrn} onChange={legalOgrnChangeHandle}/>
                                </div>
                                <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                    <div className="interactable--label">Скан ОГРН*</div>
                                    <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={legal.ogrnScan} onChange={legalOgrnScanChangeHandle}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 col-md-4" style={{marginBottom: 24}}>
                            <div className="interactable--label">Скан выписки из ЕГРИП (не старше 3 месяцев)*</div>
                            <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={legal.egripScan} onChange={legalEgripScanChangeHandle}/>
                        </div>
                        <div className="col-12 col-md-8" style={{marginBottom: 24}}>
                            <div className="interactable--label">Скан договора аренды помещения (офиса)*</div>
                            <div className="row align-items-center">
                                <div className="col-12 col-md-6">
                                    <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={legal.rentaScan} onChange={legalRentaScanChangeHandle}/>
                                </div>
                                <div className="col-12 col-md-6">
                                    <Checkbox text="Нет договора" value={legal.noRenta} onChange={legalRentaChangeHandle}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                :

                <div className="row">
                    <div className="col-12 col-md-6">
                        <div className="row">
                            <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                <div className="interactable--label">ИНН*</div>
                                <CustomInput required placeholder="хххххххххх" pattern="[0-9]{12,12}" maxLength={12} tip="Поле должно содержать 12 цифр" value={nonLegal.inn} onChange={nonlegalInnChangeHandle}/>
                            </div>
                            <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                <div className="interactable--label">Скан ИНН*</div>
                                <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={nonLegal.innScan} onChange={nonlegalInnScanChangeHandle}/>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                <div className="interactable--label">Дата регистрации*</div>
                                <CustomInput required type="date" max="9999-01-01" placeholder="дд.мм.гггг" tip="Дата должна быть указана в формате ДД.ММ.ГГГГ" value={nonLegal.registerDate} onChange={nonlegalRegisterDateChangeHandle}/>
                            </div>
                            <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                <div className="interactable--label">Скан выписки из ЕГРИП (не старше 3 месяцев)*</div>
                                <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={nonLegal.egripScan} onChange={nonlegalEgripScanChangeHandle}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-6">
                        <div className="row align-items-center">
                            <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                <div className="interactable--label">ОГРНИП*</div>
                                <CustomInput required placeholder="ххххххххххххххх" pattern="[0-9]{15,15}" maxLength={15}  tip="Поле должно содержать 15 цифр" value={nonLegal.ogrnip} onChange={nonlegalOgrnipChangeHandle}/>
                            </div>
                            <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                <div className="interactable--label">Скан ОГРНИП*</div>
                                <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={nonLegal.ogrnipScan} onChange={nonlegalOgrnipScanChangeHandle}/>
                            </div>
                        </div>

                        <div>
                            <div className="interactable--label">Скан договора аренды помещения (офиса)</div>
                                <div className="row align-items-center">
                                <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                                    <FilePicker required compressImage formats={[".jpg",".jpeg",".png"]} value={nonLegal.rentaScan} onChange={nonlegalRentaScanChangeHandle}/>
                                </div>
                                <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                                    <Checkbox text="Нет договора" value={nonLegal.noRenta} onChange={nonlegalRentaChangeHandle}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>}
            </div>
        </React.Fragment>
    )
}
export default QuestionnaireStep1;