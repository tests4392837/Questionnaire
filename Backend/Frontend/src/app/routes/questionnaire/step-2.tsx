import { useDispatch } from "react-redux";
import { addBank, removeBank, useStore } from "../../redux-storage";
import Bank from "../../models/bank";
import BankItem from "./bank-item";
import math from "../../../assets/images/icons/Math.svg"

const QuestionnaireStep2 = () => {
    const store = useStore();
    const dispatch = useDispatch();
    
    /**
     * Удаляет анкету банка. При попытке удалить анкету под индексом 0 или меньше выбросит исключение
     * @param index Индекс анкеты
     */
    function onRemoveBankHandle(index:number){
        if(index <= 0){
            throw new Error("Индекс удаляемой анкеты должен быть больше 0");
        }
        dispatch(removeBank(index));
    }
    
    /**
     * Добавляет дополнительную анкету банка
     */
    function onAddBankClickHandle(){
        dispatch(addBank());
    }
    
    const renderedBankItem = (item:Bank, index:number) => <BankItem key={index} item={item} index={index} onRemove={onRemoveBankHandle}/>

    return(
        <div>
            <div className="block-title">Банковские реквизиты</div>
            {store.routes.questionnaire.banks.map(renderedBankItem)}

            <div className="btn btn-transparent" onClick={onAddBankClickHandle}>
                <img src={math}/>
                <div>Добавить еще один банк</div>
            </div>
        </div>
    )
}
export default QuestionnaireStep2;