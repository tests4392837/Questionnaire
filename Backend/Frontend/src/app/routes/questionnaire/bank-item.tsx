import Bank from "../../models/bank";
import CustomInput from "../../../components/custom-input";
import Tooltip from "../../../components/tooltip";
import info from "../../../assets/images/icons/Info.svg"
import { setBankBic, setBankCheckingAccount, setBankCorrespondentAccount, setBankName } from "../../redux-storage";
import { useDispatch } from "react-redux";
import backend from "../../api/backend";
import { globalLoader } from "../../../components/global-loader";

interface IBankItemProps{
    item:Bank,
    index:number,
    onRemove:(index:number) => void
}
const BankItem = (props:IBankItemProps) => {
    const item = props.item;
    const dispatch = useDispatch();


    /**
     * Обработчик клика удаления банка
     */
    function onRemoveClickHandle(){
        props.onRemove(props.index);
    }

    /**
     * Обработчик изменения БИК банка
     * @param value Новое значение
     */
    function bankBicChangeHandle(value:string){
        dispatch(setBankBic({index: props.index, value: value}));
        
        if(value.length == 9){
            loadBankInfo(value);
        }
    }

    /**
     * Обработчик изменения наименования банка
     * @param value Новое значение
     */
    function bankNameChangeHandle(value:string){
        dispatch(setBankName({index: props.index, value: value}));
    }

    /**
     * Обработчик изменения рассчетного счета
     * @param value Новое значение
     */
    function bankCheckingAccountChangeHandle(value:string){
        dispatch(setBankCheckingAccount({index: props.index, value: value}));
    }

    /**
     * Обработчик изменения кор счета
     * @param value Новое значение
     */
    function bankCorrespondentAccountChangeHandle(value:string){
        dispatch(setBankCorrespondentAccount({index: props.index, value: value}));
    }

    
    /**
     * Получает и устанавливает наименование и кор счет по БИК банка
     * @param value Бик банка
     */
    function loadBankInfo(value:string){
        globalLoader.show();

        backend.getBankInfo({query: value}).then(res => {
            if(res.success){
                dispatch(setBankBic({index: props.index, value: res.data.bic}))
                dispatch(setBankName({index: props.index, value: res.data.name}));
                dispatch(setBankCorrespondentAccount({index: props.index, value: res.data.correspondentAccount}));
            }
        }).finally(() => {
            globalLoader.hide();
        });
    }

    return(
        <div className="bank-item">
            <div className="row">
                <div className="col-12 col-md-4" style={{marginBottom: 24}}>
                    <div className="interactable--label">БИК*</div>
                    <CustomInput required placeholder="ххххххххх" pattern="[0-9]{9,9}" maxLength={9} tip="Поле должно содержать 9 цифр" value={item.bic} onChange={bankBicChangeHandle}/>
                </div>
                <div className="col-12 col-md-8" style={{marginBottom: 24}}>
                    <div className="interactable--label">Название филиала банка*</div>
                    <CustomInput required placeholder="ООО «Московская промышленная компания»" value={item.name} onChange={bankNameChangeHandle}/>
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-5" style={{marginBottom: 24}}>
                    <div className="interactable--label">Рассчетный счет*</div>
                    <div className="row align-items-center no-wrap">
                        <div className="f-1">
                            <CustomInput required placeholder="хххххххххххххххххххх" pattern="[0-9]{20,20}" maxLength={20} tip="Поле должно содержать 20 цифр" value={item.checkingAccount} onChange={bankCheckingAccountChangeHandle}/>
                        </div>
                        <div>
                            <Tooltip collapseStyle={{maxWidth: 230}} anchor="right" text="Автоматическое заполнение названия филиала банка по БИК">
                                <img src={info}/>
                            </Tooltip>
                        </div>
                    </div>
                            
                </div>
                <div className="col-12 col-md-7" style={{marginBottom: 24}}>
                    <div className="interactable--label">Корреспондентский счет*</div>
                    <div className="row align-items-center no-wrap">
                        <div className="f-1">
                            <CustomInput required placeholder="хххххххххххххххххххх" pattern="[0-9]{20,20}" maxLength={20} tip="Поле должно содержать 20 цифр" value={item.correspondentAccount} onChange={bankCorrespondentAccountChangeHandle}/>
                        </div>
                        <div>
                            <Tooltip collapseStyle={{maxWidth: 230}} anchor="right" text="Автоматическое заполнение корреспондентского счета по БИК">
                                <img src={info}/>
                            </Tooltip>
                        </div>
                    </div>
                            
                </div>
            </div>
            {props.index > 0 ? <div className="btn btn-soft-danger remove-btn" onClick={onRemoveClickHandle}>Удалить</div> : <></>}
        </div>
    )
}
export default BankItem;