import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import backend from "../../api/backend";
import { useDispatch } from "react-redux";
import { clearQuestionnaireRoute, setActivityKind, setActivityKinds, useStore } from "../../redux-storage";
import QuestionnaireStep1 from "./step-1";
import { validateRegexArray } from "../../../lib/validators/regex-validator";
import QuestionnaireStep2 from "./step-2";
import CreateLegalQuestionnaireRequest from "../../api/backend/schemas/createLegalQuestionnaireRequest";
import CreateNonlegalQuestionnaireRequest from "../../api/backend/schemas/createNonlegalQuestionnaireRequest";
import { globalLoader } from "../../../components/global-loader";


const QuestionnaireRoute = () => {
    const { p_step } = useParams();
    const step = getStep();
    const store = useStore();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const currentActivityKind = useStore().routes.questionnaire.activityKind;

    const [isBusy, setIsBusy] = useState(false);

    /**
     * Получает текущий шаг
     * @returns Возвращает текущий шаг. Если в параметрах маршрута шаг отсутствует, меньше 1 или больше 2, то вернет 1
     */
    function getStep(){
        if(p_step == undefined || Number(p_step) < 1 || Number(p_step) > 2) return 1;

        return Number(p_step);
    }

    /**
     * Показывает оповещение о некорректности заполнения для шага 1
     */
    function showWrongDataMessageStep1(){
        alert("Заполните все обязательные и проверьте корректность введенных данных. Нельзя отправить некорректные ИНН, ОГРН, ОГРНИП или дату регистрации.");
    }

    /**
     * Показывает оповещение о некорректности заполнения для шага 2
     */
    function showWrongDataMessageStep2(){
        alert("Заполните все обязательные и проверьте корректность введенных данных. Нельзя отправить некорректные БИК, рассчетный счет и корр. счет.");
    }

    /**
     * Переходит к следующему состоянию страницы
     * @returns Выход из метода, если внутренние проверки не были успешны
     */
    function nextStepClickHandle(){
        if(step == 2) return;

        // проверяем корректность заполнения основной формы
        if(!baseFormIsValid()){
            showWrongDataMessageStep1();
            return;
        }

        navigate(`/questionnaire/${step + 1}`); 
    }

    /**
     * Возвращает состояние страницы на шаг назад
     * @returns Выход из метода, если уже находимся на шаге 1
     */
    function prevStepClickHandle(){
        if(step == 1) return;

        navigate(-1);
    }

    /**
     * Проверяет корректность заполнения банковских полей
     * @returns Возвращает результат проверки. Если хотя бы одно поле заполнено некорректно, то остальные проверяться не будут, вернется false
     */
    function banksFieldsIsValid(){
        store.routes.questionnaire.banks.forEach(bank => {
            var res = validateRegexArray([{value: bank.bic, pattern: "[0-9]{9,9}"}, 
                {value: bank.checkingAccount, pattern: "[0-9]{20,20}"}, 
                {value: bank.correspondentAccount, pattern: "[0-9]{20,20}"}]) && bank.name.length > 0;

            if(!res) return false;
        })
        return true;
    }

    /**
     * Проверяет корректность заполнения основной формы
     * @returns Возвращает результат проверки. Если хотя бы одно поле заполнено некорректно, то остальные проверяться не будут, вернется false
     */
    function baseFormIsValid(){
        if(!currentActivityKind) return;

        // проверка для ип
        if(currentActivityKind.isLegal){
            const legal = store.routes.questionnaire.legal;

            // проверяем корректность на стороне клиента
            if(validateRegexArray([{value: legal.inn, pattern: "[0-9]{10,10}"}, {value: legal.ogrn, pattern: "[0-9]{13,13}"}, {value: legal.registerDate, pattern: "[0-9]{4}.[0-9]{2}.[0-9]{2}"}])
             && legal.name.length > 0 && legal.shortName.length > 0 && legal.innScan && legal.ogrnScan && legal.egripScan && (legal.rentaScan || legal.noRenta)){
                
                // если на стороне клиента все нормально, то дополнительно проверяем на стороне сервера
                // TODO Возможно, это лишнее. И можно было бы оставить базовую проверку на клиенте, а на беке проверять уже всю пачку данных, а не только форму без банков
                backend.validateCompanyData({inn: legal.inn, ogrn: legal.ogrn, registerDate: legal.registerDate}).then(res => {
                    if(!res.success) return false;
                });
            } else return false;
        }

        // проверка для юр лица
        if(!currentActivityKind.isLegal){
            const nonlegal = store.routes.questionnaire.nonlegal;

            // проверяем корректность на стороне клиента
            if(validateRegexArray([{value: nonlegal.inn, pattern: "[0-9]{12,12}"}, {value: nonlegal.ogrnip, pattern: "[0-9]{15,15}"}, {value: nonlegal.registerDate, pattern: "[0-9]{4}.[0-9]{2}.[0-9]{2}"}])
             && nonlegal.innScan && nonlegal.ogrnipScan && nonlegal.egripScan && (nonlegal.rentaScan || nonlegal.noRenta)){
                
                // если на стороне клиента все нормально, то дополнительно проверяем на стороне сервера
                // TODO Возможно, это лишнее. И можно было бы оставить базовую проверку на клиенте, а на беке проверять уже всю пачку данных, а не только форму без банков
                backend.validateCompanyData({inn: nonlegal.inn, ogrn: nonlegal.ogrnip, registerDate: nonlegal.registerDate}).then(res => {
                    if(!res.success) return false;
                });
            } else return false;
        } 

        return true;
    }

    /**
     * Отправка формы на бекенд
     * @returns Выход из метода, если внутренние проверки не были успешны
     */
    function endClickHandle(){
        if(isBusy) return;

        setIsBusy(() => true);
        globalLoader.show();

        // проверяем корректность заполнения банковских полей
        if(!banksFieldsIsValid()){
            showWrongDataMessageStep2();
            return;
        }

        if(!store.routes.questionnaire.activityKind) return;

        if(store.routes.questionnaire.activityKind.isLegal){
            sendLegalQuestionnaire();
        }

        if(!store.routes.questionnaire.activityKind.isLegal){
            sendNonlegalQuestionnaire();
        }  
    }

    /**
     * Отправка формы юр лица на бекенд. Результат выполнения выводится в алерте
     */
    function sendLegalQuestionnaire(){
        const form = store.routes.questionnaire.legal;
        const model:CreateLegalQuestionnaireRequest = {
            name: form.name,
            shortName: form.shortName,
            registerDate: form.registerDate,
            inn: form.inn,
            innScan: form.innScan?.file as string,
            egripScan: form.egripScan?.file as string,
            ogrn: form.ogrn,
            ogrnScan: form.ogrnScan?.file as string,
            rentaScan: form.rentaScan?.file as string,
            noRenta: form.noRenta,
            banks: store.routes.questionnaire.banks
        }
        backend.createLegalQuestionnaire(model).then(res => {
            setIsBusy(() => false);

            if(res.success){
                alert("Форма успешно отправлена и записана в БД");
                dispatch(clearQuestionnaireRoute());
                navigate("/questionnaire/1");
            }else{
                alert(res.errorMessage);
            }
        }).catch(er => {
            setIsBusy(() => false);
            throw Error(er);
        }).finally(() => {
            globalLoader.hide();
        });
    }

    /**
     * Отправка формы ип на бекенд. Результат выполнения выводится в алерте
     */
    function sendNonlegalQuestionnaire(){
        const form = store.routes.questionnaire.nonlegal;
        const model:CreateNonlegalQuestionnaireRequest = {
            registerDate: form.registerDate,
            inn: form.inn,
            innScan: form.innScan?.file as string,
            egripScan: form.egripScan?.file as string,
            ogrnip: form.ogrnip,
            ogrnipScan: form.ogrnipScan?.file as string,
            rentaScan: form.rentaScan?.file as string,
            noRenta: form.noRenta,
            banks: store.routes.questionnaire.banks
        }
        backend.createNonlegalQuestionnaire(model).then(res => {
            setIsBusy(() => false);

            if(res.success){
                alert("Форма успешно отправлена и записана в БД");
                dispatch(clearQuestionnaireRoute());
                navigate("/questionnaire/1");
            }else{
                alert(res.errorMessage);
            }
        }).catch(er => {
            setIsBusy(() => false);
            throw Error(er);
        }).finally(() => {
            globalLoader.hide();
        });
    }

    useEffect(() => {
        globalLoader.show();
        
        backend.getActivityKinds().then(res => {
            // TODO В принципе можно сделать еще одну обертку над бекендом, 
            // которая распаковывает результат и показывает универсальный поп с информацией об ошибке, если success = false или возникло исключение
            if(res.success && res.data.length > 0){
                dispatch(setActivityKind(res.data[0]));
                dispatch(setActivityKinds(res.data));
            }
        }).finally(() => {
            globalLoader.hide();
        });
    },[]);

    useEffect(() => {
        console.log("Activity kind changed");
    },[currentActivityKind]);

    return(
        currentActivityKind ? 
        <div className="questionnaire-route">
            {step == 1 ? <QuestionnaireStep1/> : <></>}

            {step == 2 ? <QuestionnaireStep2/> : <></>}

            <div className="questionnaire-route--pagination row justify-content-end">
                {step > 1 ?
                <div onClick={prevStepClickHandle}>
                    <div className="btn btn-secondary w-lg">Назад</div>
                </div>
                :<></>}
                    
                {step < 2 ?
                <div onClick={nextStepClickHandle}>
                    <div className="btn btn-primary w-lg">Продолжить</div>
                </div>
                :
                <div onClick={endClickHandle}>
                    <div className="btn btn-primary w-lg">Завершить</div>
                </div>
                }
            </div>
        </div>
    :<></>
    )
}
export default QuestionnaireRoute;


