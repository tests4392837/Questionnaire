import ActivityKind from "../../models/activityKind";
import BankInfoResponse from "./schemas/bankInfoResponse";
import CompanyInfoResponse from "./schemas/companyInfoResponse";
import CreateLegalQuestionnaireRequest from "./schemas/createLegalQuestionnaireRequest";
import CreateNonlegalQuestionnaireRequest from "./schemas/createNonlegalQuestionnaireRequest";
import { ValidateCompanyDataRequest } from "./schemas/validateCompanyDataRequest";

class BackendApi{
    private _port = `:${window.location.port}`;
    private _host = `${window.location.hostname}${this._port.length > 1 ? this._port : ''}`;
    private _url = `https://${this._host}/backend/`

    private _headers:HeadersInit = {'Content-Type' : 'application/json; charset=UTF-8'};


    /**
     * Получение списка выдов деятельности
     * @returns Возвращает список видов деятельности
     */
    public getActivityKinds(){
        return this.get<Array<ActivityKind>>("ActivityKinds/GetList");
    }

    
    /**
     * Проверяет действительность передаваемых данных из формы
     * @param body Информация о компании из формы
     * @returns Возвращает результат проверки
     */
    public validateCompanyData(body:ValidateCompanyDataRequest){
        return this.post<null>("Questionnaire/ValidateCompany", body);
    }


    /**
     * Получает информацию о юр лице по ИНН
     * @param body Тело запроса содержащее ИНН юр лица для поиска
     * @returns Возвращает найденную информацию о юр лице
     */
    public getCompanyInfo(body:{query:string}){
        return this.post<CompanyInfoResponse>("Questionnaire/GetCompanyInfo", body);
    }


    /**
     * Получает информацию о банке по БИК банка
     * @param body Тело запроса содержащее БИК банка для поиска
     * @returns Возвращает найденную информацию о банке
     */
    public getBankInfo(body:{query:string}){
        return this.post<BankInfoResponse>("Questionnaire/GetBankInfo", body);
    }


    /**
     * Отправляет форму юр лица на бекенд
     * @param body Тело запроса содержащее форму
     * @returns Возвращает результат выполнения
     */
    public createLegalQuestionnaire(body:CreateLegalQuestionnaireRequest){
        return this.post<null>("Questionnaire/CreateLegal", body);
    }


    /**
     * Отправляет форму ип на бекенд
     * @param body Тело запроса содержащее форму
     * @returns Возвращает результат выполнения
     */
    public createNonlegalQuestionnaire(body:CreateNonlegalQuestionnaireRequest){
        return this.post<null>("Questionnaire/CreateNonlegal", body);
    }


    /**
     * Обертка над базовым fetch для отправки POST запроса
     * @param route Маршрут запроса, например questionnaire/getBankInfo
     * @param body Тело запроса представляющее объект
     * @returns Возвращает IBaseReponseData с data указанного типа в T
     */
    private async post<T>(route:string, body:object) : Promise<IBaseReponseData<T>>{
        const url = `${this._url}${route}`
        const jsonString = JSON.stringify(body);
        var data:any;

        await fetch(url, { method : 'POST', headers : this._headers, body: jsonString}).then(async response =>{
            if(response.ok){
                data = JSON.parse(await response.text());
            }else{
                throw new Error('Ошибка подключения к серверу');
            }
        }).catch(error => {
            throw new Error(error);
        });

        return data;
    }


    /**
     * Обертка над базовым fetch для отправки GET запроса
     * @param route Маршрут запроса, например questionnaire/getBankInfo
     * @returns Возвращает IBaseReponseData с data указанного типа в T
     */
    private async get<T>(route:string) : Promise<IBaseReponseData<T>>{
        const url = `${this._url}${route}`
        var data:any;

        await fetch(url, { method : 'GET', headers : this._headers}).then(async response =>{
            if(response.ok){
                data = JSON.parse(await response.text());
            }else{
                throw new Error('Ошибка подключения к серверу');
            }
        }).catch(error => {
            throw new Error(error);
        });

        return data;
    }
}

interface IBaseReponseData<T>{
    data:T,
    errorMessage:string,
    success:boolean
}

const backend = new BackendApi();
export default backend;