import Bank from "../../../models/bank";

export default interface CreateLegalQuestionnaireRequest{
    name:string,
    shortName:string,
    registerDate:string,
    inn:string,
    ogrn:string,
    innScan:string,
    ogrnScan:string,
    egripScan:string,
    rentaScan:string,
    noRenta:boolean,
    banks:Array<Bank>
}