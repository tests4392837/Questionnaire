import Bank from "../../../models/bank";

export default interface CreateNonlegalQuestionnaireRequest{
    registerDate:string,
    inn:string,
    ogrnip:string,
    innScan:string,
    ogrnipScan:string,
    egripScan:string,
    rentaScan:string,
    noRenta:boolean,
    banks:Array<Bank>
}