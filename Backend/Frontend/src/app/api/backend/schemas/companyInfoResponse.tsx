export default interface CompanyInfoResponse{
    name:string,
    shortName:string,
    ogrn:string,
    registerDate:number
}