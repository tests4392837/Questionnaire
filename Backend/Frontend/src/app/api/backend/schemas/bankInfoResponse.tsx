export default interface BankInfoResponse{
    name:string,
    bic:string,
    correspondentAccount:string
}