export interface ValidateCompanyDataRequest {
    inn:string,
    ogrn:string,
    registerDate:string
}