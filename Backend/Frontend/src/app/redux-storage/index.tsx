import { PayloadAction, configureStore, createSlice } from "@reduxjs/toolkit";
import ActivityKind from "../models/activityKind";
import { useSelector } from "react-redux";
import { IFilePickerResult } from "../../components/file-picker";
import Bank from "../models/bank";

interface IQuestionnaireRooteLegalState{
    name:string,
    shortName:string,
    registerDate:string,
    inn:string,
    ogrn:string,
    innScan:IFilePickerResult|undefined,
    ogrnScan:IFilePickerResult|undefined,
    egripScan:IFilePickerResult|undefined,
    rentaScan:IFilePickerResult|undefined,
    noRenta:boolean
}

interface IQuestionnaireRooteNonlegalState{
    inn:string,
    ogrnip:string,
    registerDate:string,
    innScan:IFilePickerResult|undefined,
    ogrnipScan:IFilePickerResult|undefined,
    egripScan:IFilePickerResult|undefined,
    rentaScan:IFilePickerResult|undefined,
    noRenta:boolean
}

interface IQuestionnaireRooteState{
    activityKind:ActivityKind|undefined,
    legal:IQuestionnaireRooteLegalState,
    nonlegal:IQuestionnaireRooteNonlegalState,
    banks:Array<Bank>
}

export interface IStoreState {
    store:{
        activityKinds: Array<ActivityKind>,
        routes:{
            questionnaire:IQuestionnaireRooteState
        }
    }
}

const InitialState : IStoreState = {
    store:{
        activityKinds: [],
        routes:{
            questionnaire:{
                activityKind:undefined,
                legal:{
                    name: "",
                    shortName: "",
                    registerDate: "",
                    inn: "",
                    ogrn: "",
                    innScan: undefined,
                    ogrnScan: undefined,
                    egripScan: undefined,
                    rentaScan: undefined,
                    noRenta: false
                },
                nonlegal:{
                    inn: "",
                    ogrnip: "",
                    registerDate: "",
                    innScan: undefined,
                    ogrnipScan: undefined,
                    egripScan: undefined,
                    rentaScan: undefined,
                    noRenta: false
                },
                banks: [{
                    bic: "",
                    name: "",
                    checkingAccount: "",
                    correspondentAccount: ""
                }]
            }
        }
    }
}

const slice = createSlice({
    name: 'default',
    initialState: InitialState,
    reducers: {
        setActivityKinds: (state, action:PayloadAction<Array<ActivityKind>>) => {
            state.store.activityKinds = [...action.payload];
        },

        // questionnaire route
        setActivityKind: (state, action:PayloadAction<ActivityKind|undefined>) => {
            state.store.routes.questionnaire.activityKind = action.payload;
        },

        setNonlegalInn: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.nonlegal.inn = action.payload;
        },
        setNonlegalOgrnip: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.nonlegal.ogrnip = action.payload;
        },
        setNonlegalRegisterDate: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.nonlegal.registerDate = action.payload;
        },
        setNonlegalInnScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.nonlegal.innScan = action.payload;
        },
        setNonlegalOgrnipScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.nonlegal.ogrnipScan = action.payload;
        },
        setNonlegalEgripScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.nonlegal.egripScan = action.payload;
        },
        setNonlegalRentaScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.nonlegal.rentaScan = action.payload;
        },
        setNonlegalRentaContract: (state, action:PayloadAction<boolean>) => {
            state.store.routes.questionnaire.nonlegal.noRenta = action.payload;
        },
        

        setLegalInn: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.legal.inn = action.payload;
        },
        setLegalName: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.legal.name = action.payload;
        },
        setLegalShortName: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.legal.shortName = action.payload;
        },
        setLegalOgrn: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.legal.ogrn = action.payload;
        },
        setLegalRegisterDate: (state, action:PayloadAction<string>) => {
            state.store.routes.questionnaire.legal.registerDate = action.payload;
        },
        setLegalInnScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.legal.innScan = action.payload;
        },
        setLegalOgrnScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.legal.ogrnScan = action.payload;
        },
        setLegalEgripScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.legal.egripScan = action.payload;
        },
        setLegalRentaScan: (state, action:PayloadAction<IFilePickerResult|undefined>) => {
            state.store.routes.questionnaire.legal.rentaScan = action.payload;
        },
        setLegalRentaContract: (state, action:PayloadAction<boolean>) => {
            state.store.routes.questionnaire.legal.noRenta = action.payload;
        },
        
        addBank: (state, action:PayloadAction) => {
            state.store.routes.questionnaire.banks.push({bic: "", name: "", correspondentAccount: "", checkingAccount: ""});
        },
        removeBank: (state, action:PayloadAction<number>) => {
            if(action.payload > -1 && action.payload <= state.store.routes.questionnaire.banks.length - 1){
                state.store.routes.questionnaire.banks.splice(action.payload, 1);
            }
        },
        setBankBic: (state, action:PayloadAction<{index:number, value:string}>) => {
            if(action.payload.index > -1 && action.payload.index <= state.store.routes.questionnaire.banks.length - 1){
                state.store.routes.questionnaire.banks[action.payload.index].bic = action.payload.value;
            }
        },
        setBankName: (state, action:PayloadAction<{index:number, value:string}>) => {
            if(action.payload.index > -1 && action.payload.index <= state.store.routes.questionnaire.banks.length - 1){
                state.store.routes.questionnaire.banks[action.payload.index].name = action.payload.value;
            }
        },
        setBankCheckingAccount: (state, action:PayloadAction<{index:number, value:string}>) => {
            if(action.payload.index > -1 && action.payload.index <= state.store.routes.questionnaire.banks.length - 1){
                state.store.routes.questionnaire.banks[action.payload.index].checkingAccount = action.payload.value;
            }
        },
        setBankCorrespondentAccount: (state, action:PayloadAction<{index:number, value:string}>) => {
            if(action.payload.index > -1 && action.payload.index <= state.store.routes.questionnaire.banks.length - 1){
                state.store.routes.questionnaire.banks[action.payload.index].correspondentAccount = action.payload.value;
            }
        },
        

        clearQuestionnaireRoute: (state, action:PayloadAction) => {
            state.store.routes.questionnaire = {...InitialState.store.routes.questionnaire, activityKind: state.store.routes.questionnaire.activityKind}
        }
    }
})


export const store = configureStore({
    reducer: slice.reducer
});

export const useStore = () => useSelector((state:IStoreState) => state.store);

// Экшены для диспатча
export const { setActivityKinds, setLegalInn, setLegalName, 
    setLegalShortName, setLegalEgripScan, setLegalInnScan, setLegalOgrn, 
    setLegalOgrnScan, setLegalRegisterDate, setLegalRentaContract, setLegalRentaScan,
    setNonlegalEgripScan, setNonlegalInn, setNonlegalInnScan, setNonlegalOgrnip, setNonlegalOgrnipScan,
    setNonlegalRegisterDate, setNonlegalRentaContract, setNonlegalRentaScan, setBankBic,
    setBankCheckingAccount, setBankCorrespondentAccount, setBankName, addBank, removeBank, clearQuestionnaireRoute,
    setActivityKind } = slice.actions