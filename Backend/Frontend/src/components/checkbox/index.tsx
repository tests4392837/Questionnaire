import { IChangeableProps } from "../types";

interface ICheckboxProps extends IChangeableProps<boolean> {
    text:string
}

const Checkbox = ({...props}:ICheckboxProps) => {
    function changeHandler(){
        if(props.value == undefined || !props.onChange) return;

        let value = !props.value;
        props.onChange(value);
    }

    return(
        <div className={props.value ? "checkbox checked" : "checkbox"} onClick={changeHandler}>
            <div className="checkbox--box">
                <div className="chekbox--box--icon">✓</div>
            </div>
            <div className="checkbox--text">{props.text}</div>
        </div>
    )
}
export default Checkbox;