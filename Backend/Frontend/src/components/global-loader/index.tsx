import { useEffect, useState } from "react";

interface IGlobalLoaderControllerListener{
    type:"hide"|"show",
    action:Function
}
class GlobalLoaderController{
    private listeners:Array<IGlobalLoaderControllerListener> = [];


    addListener = (type:"show"|"hide", listener:Function) => {
        this.listeners.push({type: type, action: listener});
    }

    removeListener = (listener:Function) => {
        for(var i = 0; i < this.listeners.length; i++){
            if(this.listeners[i].action == listener){
                this.listeners.splice(i, 1);
                break;
            }
        }
    }

    show = () => {
        for(var i = 0; i < this.listeners.length; i++){
            if(this.listeners[i].type == 'show'){
                this.listeners[i].action();
            }
        }
    }

    hide = () => {
        for(var i = 0; i < this.listeners.length; i++){
            if(this.listeners[i].type == 'hide'){
                this.listeners[i].action();
            }
        }
    }
}

const controller = new GlobalLoaderController();
export const globalLoader = {
    show:controller.show,
    hide:controller.hide
}

export const GlobalLoaderProvider = () => {
    const [active, setActive] = useState(false);

    function showHandle(){
        setActive(() => true);
    }

    function hideHandle(){
        setActive(() => false);
    }

    useEffect(() => {
        controller.addListener('show', showHandle);
        controller.addListener('hide', hideHandle);

        return () => {
            controller.removeListener(showHandle)
            controller.removeListener(hideHandle)
        }
    },[]);

    return(
        <div className={active ? "global-loader active" : "global-loader"}>
            <div className="indicator"></div>
        </div>
    )
}