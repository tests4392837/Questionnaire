import { IChangeableProps } from "../types";
import arrows from "../../assets/images/icons/Arrows.svg"
import { useEffect, useState } from "react";

interface IDropdownProps extends IChangeableProps<any>{
    placeholder:string,
    list:Array<any>,
    datatemplate:(item:any) => string|JSX.Element
}

const Dropdown = ({...props}:IDropdownProps) => {
    const [state, setState] = useState({
        active: false
    });

    function clickHandler(){
        let value = !state.active;

        // Выполняем с задержкой, чтобы избежать перекрытия document click event
        setTimeout(() => {
            setState(state => {
                state.active = value;
                return {...state}
            })
        }, 50);
    }

    function documentClickHandler(event:any){
        setState(state => {
            state.active = false;
            return {...state}
        })
    }

    function itemClickHandler(item:any){
        setState(state => {
            state.active = false;
            return {...state}
        });

        if(props.onChange){
            props.onChange(item);
        } 
    }
    
    function renderedItem(item:any, index:number){
        return(
            <ListItem key={index} item={item} datatemplate={props.datatemplate} onClick={itemClickHandler}></ListItem>
        )
    }

    useEffect(() => {
        document.addEventListener('click', documentClickHandler);

        return () => {
            document.removeEventListener('click', documentClickHandler);
        }
    },[state.active]);

    return(
        <div className={state.active ? "dropdown-list interactable active" : "dropdown-list interactable"}>
            <div className="dropdown-list--selected" onClick={clickHandler}>
                <div className={props.value ? "dropdown-list--selected--text selected" : "dropdown-list--selected--text"}>
                    {props.value ? props.datatemplate(props.value) : props.placeholder}
                </div>
                <div className="dropdown-list--selected--arrow">
                    <img src={arrows}/>
                </div>
            </div>
            <div className="dropdown-list--collapse" aria-hidden>
                <div className="dropdown-list--collapse--list">
                    {props.list.map(renderedItem)}
                </div>
            </div>
        </div>
    )
}
export default Dropdown;


interface IListItemProps {
    item:any,
    onClick:(item:any) => void,
    datatemplate:(item:any) => string|JSX.Element
}
const ListItem = (props:IListItemProps) => {
    function clickHandler(event:any){
        props.onClick(props.item);
        event.stopPropagation();
    }

    return(
        <div onClick={clickHandler}>{props.datatemplate(props.item)}</div>
    )
}