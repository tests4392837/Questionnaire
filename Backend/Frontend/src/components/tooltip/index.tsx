interface ITooltipProps{
    text:string,
    children:JSX.Element,
    anchor?:"left"|"center"|"right",
    className?:string
    collapseStyle?:React.CSSProperties
}
const Tooltip = ({anchor = "center", className = "", ...props}:ITooltipProps) => {
    return(
        <div className={`tooltip ${anchor} ${className}`}>
            {props.children}
            <div className="tooltip--collapse" style={props.collapseStyle}>
                {props.text}
            </div>
        </div>
    )
}
export default Tooltip;