export interface IChangeableProps<T>{
    required?:boolean,
    value?:T,
    onChange?:(value:T) => void
}