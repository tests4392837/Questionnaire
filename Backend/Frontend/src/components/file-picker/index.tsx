import { IChangeableProps } from "../types";
import { useEffect, useRef } from "react";
import upload from "../../assets/images/icons/Upload.svg"
import cross from "../../assets/images/icons/cross1.svg"

export interface IFilePickerResult{
    name:string,
    file:string
}
interface IFilePickerProps extends IChangeableProps<IFilePickerResult|undefined> {
    placeholder?:string,
    formats:Array<string>,
    compressImage?:boolean
}
const FilePicker = ({placeholder = "Выберите или перетащите файл", ...props}:IFilePickerProps) => {
    const nameFieldRef = useRef<HTMLDivElement>(null);

    const input = getInput();
    const reader = getReader();

    function getInput(){
        const input = document.createElement('input');
        input.type = 'file';
        input.accept = props.formats.join(",");
        input.onchange = handleInputChange;

        return input;
    }

    function getReader(){
        const reader = new FileReader();

        return reader;
    }

    function isCorrectFormat(name:string){
        const nameParts = name.split(".");

        if(nameParts.length != 2) return false;

        return props.formats.some(i => i == `.${nameParts[0]}`)
    }

    function setConvertedFileName(){
        if(!props.value || !nameFieldRef.current) return;

        const nameParts = props.value.name.split(".");
        if(nameParts.length != 2) return;

        const format = nameParts[1];
        const tempContainer = document.createElement('div');
        var name = nameParts[0];
        var sliced = false;
        
        
        tempContainer.textContent = name;
        tempContainer.style.whiteSpace = "nowrap";
        tempContainer.style.width = "max-content";
        document.body.appendChild(tempContainer);
        
        var conter = name.length - 1;
        while(tempContainer.clientWidth > nameFieldRef.current.clientWidth - 40){
            tempContainer.textContent = name;

            if(name.length < 3){
                break;
            }

            name = name.substring(0, conter);
            sliced = true;
            conter --;
        }
        tempContainer.remove();

        nameFieldRef.current.textContent = sliced ? `${name}....${format}` : `${name}.${format}`;
    }

    function handleClearClick(){
        if(!props.onChange) return;

        props.onChange(undefined);
    }

    function handleSelectClick(){
        input.click();
    }

    function handleInputChange(event:any){
        if(!event.target || !event.target.files) return;

        var file = event.target.files[0];
        
        if(!file || isCorrectFormat(file.name)) return;

        reader.onload = (readerEvent:ProgressEvent<FileReader>) => {
            if(!readerEvent.target || readerEvent.target.result == null || !props.onChange) return;

            var base64 = readerEvent.target.result as string;
            if(props.compressImage && (base64.includes("jpg") || base64.includes("png") || base64.includes("jpeg"))){
                var image = new Image();
                image.src = base64;
                image.onload = () => {
                    if(props.onChange){
                        if(file.size / 1024 > 500 && image.width > 1600 && image.height > 900){
                            base64 = getCompressedImage(base64, image.width / 2, image.height / 2);
                            image.remove();
                        }
                        props.onChange({name: file.name, file: base64});
                    }
                }
            }else{
                props.onChange({name: file.name, file: base64});
            }
            
        }

        reader.readAsDataURL(file);
    }

    useEffect(() => {
        setConvertedFileName();
    },[props.value, nameFieldRef]);

    return(
        <div className="file-picker interactable">
            {!props.value ?
            <div className="file-picker--placeholder" onClick={handleSelectClick}>
                <div className="file-picker--placeholder--text">{placeholder}</div>
                <div className="file-picker--placeholder--icon">
                    <img src={upload}/>
                </div>
            </div>
            :<></>}
            <div className="file-picker--selected">
                <div className="file-picker--selected--icon">✓</div>
                <div ref={nameFieldRef} className="file-picker--selected--name"></div>
                <div className="file-picker--selected--clear" onClick={handleClearClick}>
                    <img src={cross}/>
                </div>
            </div>
        </div>
    )
}
export default FilePicker;


function getCompressedImage(base64:string, width:number, height:number){
    var image = document.createElement('img');
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');

    image.src = base64;
    canvas.width = width;
    canvas.height = height;

    if(context){
        context.drawImage(image, 0, 0, width, height);
        var res = canvas.toDataURL("image/jpeg", 1);
        image.remove();
        canvas.remove();
        return res;
    }else{
        image.remove();
        canvas.remove();
        throw Error("Ошибка создания canvas context");
    }
}