import { IChangeableProps } from "../types";

interface ICustomInputProps extends IChangeableProps<string> {
    placeholder?:string,
    maxLength?:number,
    pattern?:string
    tip?:string,
    type?:React.HTMLInputTypeAttribute,
    min?:string|number,
    max?:string|number
}
const CustomInput = ({...props}:ICustomInputProps) => {
    function changeHandler(event:any){
        if(!props.onChange) return;

        props.onChange(event.target.value);
    }

    return(
        <div className="custom-input interactable">
            <input type={props.type} pattern={props.pattern} min={props.min} max={props.max} maxLength={props.maxLength} placeholder={props.placeholder} value={props.value} onChange={changeHandler}/>
            {props.tip ? <div className="custom-input--tip">{props.tip}</div> : <></>}
        </div>
    )
}
export default CustomInput;


