﻿using Backend.Context.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Context {
    public class SqliteContext : DbContext {
        public SqliteContext(DbContextOptions<SqliteContext> options) : base(options) {
            Database.EnsureCreated();
        }

        public DbSet<ActivityKind> ActivityKinds { get; set; }
        public DbSet<Questionnaire> Questionnaires { get; set; }
        public DbSet<QuestionnaireBank> QuestionnaireBanks { get; set; }
    }
}
