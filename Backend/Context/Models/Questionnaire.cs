﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Context.Models {
    public class Questionnaire {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// ОГРН. Только для ЮР
        /// </summary>
        public string? Ogrn { get; set; }

        /// <summary>
        /// ОГРНИП. Только для ИП
        /// </summary>
        public string? Ogrnip { get; set; }

        /// <summary>
        /// Наименование полное. Только для юр
        /// </summary>
        public string? Name { get; set; }

        /// <summary>
        /// Наименование сокращенное. Только для юр
        /// </summary>
        public string? ShortName { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public string RegisterDate { get; set; }

        /// <summary>
        /// Есть ли договор аренды
        /// </summary>
        public bool NoRenta { get; set; }


        // Фотографии

        /// <summary>
        /// Файл изображения скана ИНН
        /// </summary>
        public string InnScanFileName { get; set; }

        /// <summary>
        /// Файл изображения скана ОГРН. Только для юр
        /// </summary>
        public string? OgrnScanFileName { get; set; }

        /// <summary>
        /// Файл изображения скана ОГРНИП. Только для ип
        /// </summary>
        public string? OgrnIpScanFileName { get; set; }

        /// <summary>
        /// Файл изображения скана ЕГРИП
        /// </summary>
        public string EgripScanFileName { get; set; }

        /// <summary>
        /// Скан договора аренды офиса
        /// </summary>
        public string? RentaScanFileName { get; set; }


        /// <summary>
        /// Список банковских реквизитов
        /// </summary>
        public List<QuestionnaireBank> Banks { get; set; }
    }
}
