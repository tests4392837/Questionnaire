﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Context.Models {
    public class QuestionnaireBank {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// БИК
        /// </summary>
        public string Bic { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Рассчетный счет
        /// </summary>
        public string CheckingAccount { get; set; }

        /// <summary>
        /// Корреспондентский счет
        /// </summary>
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// Связь с анкетой в БД
        /// </summary>
        public Guid QuestionnaireId { get; set; }
        [ForeignKey(nameof(QuestionnaireId))]
        public Questionnaire? Questionnaire { get; set; }
    }
}
