﻿using System.ComponentModel.DataAnnotations;

namespace Backend.Context.Models {
    public class ActivityKind {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Является ли юр лицом (ООО или ОАО, например)
        /// </summary>
        public bool IsLegal { get; set; }
    }
}
