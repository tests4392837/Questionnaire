﻿namespace Backend.Lib.Extentions {
    public static class DateTimeExtentions {
        private static readonly long UnixEpochTicks = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;

        public static long? ToJavaScriptTicks(this DateTime? value) {
            return value == null ? null : (value.Value.ToUniversalTime().Ticks - UnixEpochTicks) / 10000;
        }

        public static long ToJavaScriptTicks(this DateTime value) {
            return (value.ToUniversalTime().Ticks - UnixEpochTicks) / 10000;
        }
    }
}
