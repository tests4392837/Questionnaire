﻿namespace Backend.Lib.OC {
    public static class FileManager {
        /// <summary>
        /// Сохраняет файл в файловую систему и возвращает уникальное наименование. Наименование файла генерируется через <see cref="Guid"/>
        /// </summary>
        /// <param name="file">Файл представленный в base64 строке</param>
        /// <param name="directory">Директория в которой нужно сохранить файл</param>
        /// <param name="format">Формат в котором нужно сохранить файл</param>
        /// <remarks>Удаляет метадату из base64 строки, если она содержится</remarks>
        /// <returns>Возвращает уникальное наименование файла</returns>
        public static string SaveFile(string base64, string directory, string format) {
            int separatorIndex = base64.IndexOf(',');
            int startIndex = separatorIndex != -1 ? separatorIndex + 1 : 0;
            base64 = base64.Substring(startIndex);

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string name = GetFileName(directory, format);
            string path = Path.Combine(directory, name);
            
            using (BinaryWriter writer = new(File.Create(path))) {
                writer.Write(Convert.FromBase64String(base64));
            }

            return name;
        }

        /// <summary>
        /// Возвращает уникальное наименование файла
        /// </summary>
        /// <returns></returns>
        private static string GetFileName(string directory, string format) {
            string name = $"{Guid.NewGuid()}{format}";
            string path = Path.Combine(directory, name);

            while (File.Exists(path)) {
                name = $"{Guid.NewGuid()}{format}";
            }
            return name;
        }
    }
}
