﻿using Backend.Context;
using DaDataApiService.Util;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;

namespace Backend {
    public class Startup {
        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment) {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddResponseCompression(options => options.EnableForHttps = true);

            services.AddControllers()
                .AddNewtonsoftJson();

            services.AddDadataApi("d6221010c4c5837ed0c0b0db209e8a04843e032e");

            services.AddSpaStaticFiles(configuration => {
                configuration.RootPath = "Frontend/build";
            });

            services.AddDbContext<SqliteContext>(options => {
                string connection = Configuration.GetConnectionString("sqlite-dev-db");
                options.UseSqlite(connection);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            app.UseResponseCompression();

            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseHsts();

            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa => {
                spa.Options.SourcePath = "Frontend";

                if (Environment.IsDevelopment()) {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
