﻿using Backend.Controllers.Schemas;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers {
    public class ExtendedControllerBase : ControllerBase {
        public OkObjectResult Success() {
            return Ok(new BaseResponse(true));
        }


        public OkObjectResult Success(object data) {
            return Ok(new BaseResponse(true, data));
        }


        public OkObjectResult Error(string message) {
            return Ok(new BaseResponse(false, message));
        }
    }
}
