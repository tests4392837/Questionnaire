﻿using Backend.Context;
using Backend.Context.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers {
    [ApiController]
    [Route("backend/[controller]/[action]/{id?}")]
    public class ActivityKindsController : ExtendedControllerBase {
        private readonly SqliteContext _db;

        public ActivityKindsController(SqliteContext db) {
            _db = db;
        }


        /// <summary>
        /// Возвращает список видов деятельности
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<ActivityKind>> GetList() {
            FillTables();

            var list = _db.ActivityKinds
                .OrderBy(i => i.IsLegal)
                .AsNoTracking();

            return Success(list);
        }


        /// <summary>
        /// Первичное заполнение таблиц, если они еще пустые
        /// </summary>
        /// <remarks>Быстрее так, чем руками</remarks>
        private void FillTables() {
            bool someTablesFilled = false;
            if (!_db.ActivityKinds.Any()) {
                _db.ActivityKinds.AddRange(new List<ActivityKind>() {
                    new ActivityKind() {
                        Name = "Индивидуальный предприниматель (ИП)",
                        IsLegal = false
                    },
                    new ActivityKind() {
                        Name = "Общество с ограниченной ответственностью (ООО)",
                        IsLegal = true
                    }
                });
                someTablesFilled = true;
            }

            if (someTablesFilled) {
                _db.SaveChanges();
            }
        }
    }
}
