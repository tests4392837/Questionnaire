﻿using Backend.Context;
using Backend.Context.Models;
using Backend.Controllers.Schemas.QuestionnaireControllerSchemas;
using Backend.Lib.OC;
using Dadata.Model;
using DaDataApiService;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace Backend.Controllers {
    [ApiController]
    [Route("backend/[controller]/[action]/{id?}")]
    public class QuestionnaireController : ExtendedControllerBase {
        private readonly DadataApi _dadataApi;
        private readonly IWebHostEnvironment _environment;
        private readonly SqliteContext _db;

        public QuestionnaireController(DadataApi dadataApi, IWebHostEnvironment environment, SqliteContext db) {
            _dadataApi = dadataApi;
            _environment = environment;
            _db = db;
        }

        /// <summary>
        /// Поиск банков через dadata api
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private async Task<SuggestResponse<Bank>> GetBanks(string query, BankType type) {
            var banks = await _dadataApi.suggestAsync.SuggestBank(new SuggestBankRequest(query) {
                query = query,
                type = new BankType[] { type }
            });
            return banks;
        }


        /// <summary>
        /// Поиск компаний через dadata api
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private async Task<SuggestResponse<Party>> GetCompanies(string query) {
            var companies = await _dadataApi.suggestAsync.SuggestParty(query);
            return companies;
        }


        /// <summary>
        /// Возвращает компанию найденную по ИНН, если нет, то возвращает неуспешный результат в статусе 200
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<GetCompanyInfoResponse>> GetCompanyInfo(GetCompanyInfoRequest model) {
            // Ищем компании по данным в модели
            var companies = await GetCompanies(model.Query);

            if (companies.suggestions.Count == 0) return Error("Не найдено компаний для указанных данных");

            // В задании нет уточнения, нужно ли создавать дроп для выбора нужной организации по инн, поэтому берем первую, а не возвращаем весь список
            // Но по-хорошему делать весь список, конечно
            var suggestion = companies.suggestions[0];

            var response = new GetCompanyInfoResponse(
                suggestion.value,
                suggestion.unrestricted_value,
                suggestion.data.ogrn,
                suggestion.data.state.registration_date.Value.ToJavaScriptTicks());


            return Success(response);
        }


        /// <summary>
        /// Сверяет введенные данные в форме на шаге 1 и возвращает результат. Ключевым параметром является Inn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> ValidateCompany(ValidateCompanyResponse model) {
            // Ищем компании по данным в модели
            var companies = await GetCompanies(model.Inn);

            if (companies.suggestions.Count == 0) return Error("Указаны неверные данные");

            var suggestion = companies.suggestions[0];
            bool result = suggestion.data.inn == model.Inn && suggestion.data.ogrn == model.Ogrn && suggestion.data.state.registration_date.Value.ToString("yyyy-MM-dd") == model.RegisterDate;

            if(!result) return Error("Указаны неверные данные");

            return Success();
        }


        /// <summary>
        /// Возвращает банк найденный по ИНН, если нет, то возвращает неуспешный результат в статусе 200
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<GetBankInfoResponse>> GetBankInfo(GetBankInfoRequest model) {
            // Ищем банки по данным в модели (именно филиалы банков)
            var banks = await GetBanks(model.Query, BankType.BANK_BRANCH);

            if (banks.suggestions.Count == 0) return Error("Указаны неверные данные");

            var bank = banks.suggestions[0];

            var response = new GetBankInfoResponse(bank.data.name.payment, bank.data.bic, bank.data.correspondent_account);

            return Success(response);
        }


        /// <summary>
        /// Валидация указанных параметров и реквизитов банков через dadata api
        /// </summary>
        /// <param name="inn">ИНН</param>
        /// <param name="ogrn">ОГРН или ОГРНИП</param>
        /// <param name="registerDate">Дата регистрации</param>
        /// <param name="banks">Список банков для проверки реквизитов</param>
        /// <returns>Возвращает результат проверки true или false</returns>
        private async Task<bool> FormIsValid(string inn, string ogrn, string registerDate, List<BankModelRequest> banks) {
            bool companyIsValid = true;
            bool banksIsValid = true;

            // создаем список асинхронных экшенов для распараллеливания
            List<Func<Task>> tasks = new() {
                new (async () => {
                    var res = await GetCompanies(inn);
                    if(res.suggestions.Count > 0) {
                        var suggestion = res.suggestions[0];
                        companyIsValid = suggestion.data.inn == inn && suggestion.data.ogrn == ogrn && suggestion.data.state.registration_date.Value.ToString("yyyy-MM-dd") == registerDate;
                    }
                }),
                new (async () => {
                    foreach (var bank in banks) {
                        var res = await GetBanks(bank.Bic, BankType.BANK_BRANCH);
                        if(res.suggestions.Count > 0 ) {
                            var suggestion = res.suggestions[0];
                            banksIsValid = suggestion.data.bic == bank.Bic && suggestion.data.correspondent_account == bank.CorrespondentAccount;

                            if(!banksIsValid) break;
                        } else {
                            banksIsValid = false;
                            break;
                        }
                    }
                })
            };

            // паралельно запускаем список экшенов и ждем результат проверки
            await Parallel.ForEachAsync(tasks, async (item, cancellationToken) => {
                await item();
            });

            // объединяем результат проверки
            bool res = companyIsValid && banksIsValid;

            return res;
        }


        /// <summary>
        /// Сохранение карточки юр лица в БД
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateLegal(CreateLegalQuestionnaireRequest model) {
            // проверка реквизитов через dadata
            bool requisitesIsValid = await FormIsValid(model.Inn, model.Ogrn, model.RegisterDate, model.Banks);

            // проверка корректности текстовых полей
            bool formIsValid = Regex.IsMatch(model.Inn, "[0-9]{10,10}") 
                && Regex.IsMatch(model.Ogrn, "[0-9]{13,13}") 
                && Regex.IsMatch(model.RegisterDate, "[0-9]{4}-[0-9]{2}-[0-9]{2}")
                && model.Name.Length > 0
                && model.ShortName.Length > 0;

            // проверяем общий результат проверок
            if (!(requisitesIsValid && formIsValid)) return Error("Форма или банковские реквизиты содержат некорректные данные");

            string filesDirectory = Path.Combine(_environment.ContentRootPath, "FilesStorage");
            Questionnaire questionnaire = new() {
                Name = model.Name,
                ShortName = model.ShortName,
                RegisterDate = model.RegisterDate,
                Inn = model.Inn,
                Ogrn = model.Ogrn,
                InnScanFileName = FileManager.SaveFile(model.InnScan, filesDirectory, ".jpeg"),
                OgrnScanFileName = FileManager.SaveFile(model.OgrnScan, filesDirectory, ".jpeg"),
                EgripScanFileName = FileManager.SaveFile(model.EgripScan, filesDirectory, ".jpeg"),
                RentaScanFileName = model.RentaScan != null ? FileManager.SaveFile(model.RentaScan, filesDirectory, ".jpeg") : null,
                Banks = new List<QuestionnaireBank>(),
                NoRenta = model.NoRenta
            };
            foreach (var item in model.Banks) {
                questionnaire.Banks.Add(new() {
                    Bic = item.Bic,
                    Name = item.Name,
                    CheckingAccount = item.CheckingAccount,
                    CorrespondentAccount = item.CorrespondentAccount
                });
            }
            _db.Questionnaires.Add(questionnaire);
            _db.SaveChanges();
            

            return Success();
        }


        /// <summary>
        /// Сохранение карточки ип в БД
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateNonlegal(CreateNonlegalQuestionnaireRequest model) {
            // проверка реквизитов через dadata
            bool requisitesIsValid = await FormIsValid(model.Inn, model.Ogrnip, model.RegisterDate, model.Banks);

            // проверка корректности текстовых полей
            bool formIsValid = Regex.IsMatch(model.Inn, "[0-9]{12,12}")
                && Regex.IsMatch(model.Ogrnip, "[0-9]{15,15}")
                && Regex.IsMatch(model.RegisterDate, "[0-9]{4}-[0-9]{2}-[0-9]{2}");

            // проверяем общий результат проверок
            if (!(requisitesIsValid && formIsValid)) return Error("Форма или банковские реквизиты содержат некорректные данные");

            string filesDirectory = Path.Combine(_environment.ContentRootPath, "FilesStorage");
            Questionnaire questionnaire = new() {
                RegisterDate = model.RegisterDate,
                Inn = model.Inn,
                Ogrnip = model.Ogrnip,
                InnScanFileName = FileManager.SaveFile(model.InnScan, filesDirectory, ".jpeg"),
                OgrnIpScanFileName = FileManager.SaveFile(model.OgrnipScan, filesDirectory, ".jpeg"),
                EgripScanFileName = FileManager.SaveFile(model.EgripScan, filesDirectory, ".jpeg"),
                RentaScanFileName = model.RentaScan != null ? FileManager.SaveFile(model.RentaScan, filesDirectory, ".jpeg") : null,
                Banks = new List<QuestionnaireBank>(),
                NoRenta = model.NoRenta
            };
            foreach (var item in model.Banks) {
                questionnaire.Banks.Add(new() {
                    Bic = item.Bic,
                    Name = item.Name,
                    CheckingAccount = item.CheckingAccount,
                    CorrespondentAccount = item.CorrespondentAccount
                });
            }
            _db.Questionnaires.Add(questionnaire);
            _db.SaveChanges();


            return Success();
        }
    }
}
