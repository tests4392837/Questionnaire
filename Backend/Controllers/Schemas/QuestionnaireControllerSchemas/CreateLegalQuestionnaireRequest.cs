﻿namespace Backend.Controllers.Schemas.QuestionnaireControllerSchemas {
    public class CreateLegalQuestionnaireRequest {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string RegisterDate { get; set; }
        public string Inn { get; set; }
        public string Ogrn { get; set; }
        public bool NoRenta { get; set; }

        // Фотографии
        public string InnScan { get; set; }
        public string OgrnScan { get; set; }
        public string EgripScan { get; set; }
        public string? RentaScan { get; set; }

        public List<BankModelRequest> Banks { get; set; }
    }
}
