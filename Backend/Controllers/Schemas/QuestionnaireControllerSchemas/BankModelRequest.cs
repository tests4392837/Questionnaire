﻿namespace Backend.Controllers.Schemas.QuestionnaireControllerSchemas {
    public class BankModelRequest {
        public string Name { get; set; }
        public string Bic { get; set; }
        public string CheckingAccount { get; set; }
        public string CorrespondentAccount { get; set; }
    }
}
