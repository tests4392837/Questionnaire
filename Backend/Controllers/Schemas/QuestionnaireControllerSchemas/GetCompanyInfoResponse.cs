﻿namespace Backend.Controllers.Schemas.QuestionnaireControllerSchemas {
    public class GetCompanyInfoResponse {
        /// <summary>
        /// Полное наименование
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Сокращенное наименование
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Огрн
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public long RegisterDate { get; set; }

        public GetCompanyInfoResponse(string name, string shortName, string ogrn, long registerDate) {
            Name = name;
            ShortName = shortName;
            Ogrn = ogrn;
            RegisterDate = registerDate;
        }
    }
}
