﻿namespace Backend.Controllers.Schemas.QuestionnaireControllerSchemas {
    public class ValidateCompanyResponse {
        public string Inn { get; set; }
        public string Ogrn { get; set; }
        public string RegisterDate { get; set; }
    }
}
