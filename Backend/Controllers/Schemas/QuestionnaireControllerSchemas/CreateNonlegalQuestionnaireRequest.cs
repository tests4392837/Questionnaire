﻿namespace Backend.Controllers.Schemas.QuestionnaireControllerSchemas {
    public class CreateNonlegalQuestionnaireRequest {
        public string RegisterDate { get; set; }
        public string Inn { get; set; }
        public string Ogrnip { get; set; }
        public bool NoRenta { get; set; }

        // Фотографии
        public string InnScan { get; set; }
        public string OgrnipScan { get; set; }
        public string EgripScan { get; set; }
        public string? RentaScan { get; set; }

        public List<BankModelRequest> Banks { get; set; }
    }
}
