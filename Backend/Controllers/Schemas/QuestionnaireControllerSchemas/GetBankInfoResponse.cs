﻿namespace Backend.Controllers.Schemas.QuestionnaireControllerSchemas {
    public class GetBankInfoResponse {
        public string Name { get; set; }

        public string Bic { get; set; }

        public string CorrespondentAccount { get; set; }

        public GetBankInfoResponse(string name, string bic, string сorrespondentAccount) {
            Name = name;
            Bic = bic;
            CorrespondentAccount = сorrespondentAccount;
        }
    }
}
