﻿namespace Backend.Controllers.Schemas {
    /// <summary>
    /// Стандартное тело ответа для этого API сервиса
    /// </summary>
    public class BaseResponse {
        /// <summary>
        /// Успешность операции
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Возвращаемые данные
        /// </summary>
        public object? Data { get; set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string? ErrorMessage { get; set; }

        private BaseResponse() { }

        public BaseResponse(bool success) {
            Success = success;
        }

        public BaseResponse(bool success, string errorMessage) {
            Success = success;
            ErrorMessage = errorMessage;
        }

        public BaseResponse(bool success, object? data) {
            Success = success;
            Data = data;
        }

        public BaseResponse(bool success, object? data, string errorMessage) {
            Success = success;
            Data = data;
            ErrorMessage = errorMessage;
        }
    }
}
